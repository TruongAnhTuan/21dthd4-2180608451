# 21DTHD4-2180608451
## Trương Anh Tuấn 
## Lớp : 21DTHD4
## MSSV:2180608451
## Khoa : CNTT

| title                 | Cashier views selected seats   | 
| -------------         |:-------------:| 
| Value statement       |   As a Cinema Cashier : I want when I choose a seat it will turn red and when the customer has paid it will turn yellow |
| Acceptance Criteria   |   Acceptance Criteria :When customers make a choice, the cashier must make sure the seat they choose is available based on the seat color that has changed.Then the seat turns red, it notifies the customer that the seat is empty and the seats are already yellow indicates that the seat has been sold.|
|Definition of Done     | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner                  | Mr.Tuan|

![Cashier views selected seats](https://gitlab.com/TruongAnhTuan/21dthd4-2180608451/-/raw/main/Cashier%20views%20selected%20seats.png?ref_type=heads)

| No | ReqID | Test Objective| Test Steps| Expected Result| Actual Result| 
|------|-------|-------------|----------| ------------| ---------|
| 1 | Req01     | Check if Cashier can see the list of seats that have been selected   |  1: Cashier logs in to the system <br> 2:  Cashier selects the "View selected seat" function |The list of seats that have been selected is displayed correctly and fully| |
| 2 | Req02     | Check if Cashier can see the details of each seat selected  | 1: Cashier logs in to the system <br> 2: Cashier selects the "View selected seat" function  <br> 3: Cashier chooses a seat from the list|  Details of the selected seat are displayed properly and fully | |
|  3| Req03     | Check if Cashier can update the status of the selected chair | 1:  Cashier logs in to the system <br> 2:  Cashier selects the "View selected seat" function  <br> 3: Cashier selects a seat from the list and updates its status| The status of the seat is successfully updated in the system | |  selected|                                               
# 21DTHD4-2180608710
## Nguyễn Nhựt Minh
## Lớp : 21DTHD4
## MSSV:2180608710
## Khoa : CNTT

## Nguyễn Nhựt Minh
|Title:               | Cashier Logs in|
|:-------------------:|-----------------------------------------------------------------------------------------------------------------------------------------:|
|Values Statement     | As a cashier, I want when I log in, it will take me to the page I work on, so I can grasp my work.                                                    |
|Acceptance Criteria  | Acceptance Criteria: When I logged into the software with my employee ID the system confirmed I was the cashier and entered my workplace.|
|Difinition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mr Minh|
|Interation | |
|Estimate | |
![Cashier Logs in ](https://gitlab.com/TruongAnhTuan/21dthd4-2180608451/-/raw/main/Cashier_Login.png?ref_type=heads)
# 21DTHD4-2180607948
# Nguyễn Hữu Quý 
# Lớp: 21DTHD4
# MSSV: 2180607948
# Khoa: CNTT

| Title | Cashier confirms customer has paid  |
| --- | --- |
| Value Statement | As a Cashier of the Cinema system, I want a page which helps me to identify when customers have paid so that I can support them in the next step |
| Acceptance Criteria | <ins>Acceptance Criteria 4:</ins><br/>Given that the customer chose the seats they had ordered <br/> Then the staff will confirm the payment method which the customer has chosen<br/> And ensure that the customers have tranfered money or paid by cash |
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mr. Quy|
|Interation | |
|Estimate | |
![Cashier confirms customer has paid](https://gitlab.com/no-name9340436/21dthd4-2180607948/-/raw/main/Pay.png?ref_type=heads)
# Võ Quốc Tân
# 2180608423
# 21DTHD4
# CNTT

## Võ Quốc Tân
| Title | Cashier find movie |
| --- | --- |
| Value Statement | As a cashier, I want to find the right movie according to the customer's request |
| Acceptance Criteria | <ins> Acceptance Criteria 2: </ins><br/>Movies are selected based on customer requests.<br/>After that, show the customer's selected movie information.</br> The customer will check the movie information again.
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mr. Tan |
|Interation | |
|Estimate | |
![Cashier find Movie](https://gitlab.com/quoctan1/21dthd4-2180608423/-/raw/main/PictureFind.png?ref_type=heads)
## Phạm Thị Thu Huyền
## 21DTHD4
## 2180609318
## CNTT
| Title | The cashier transfers data to the printer to print tickets |
| --- | --- |
| Value proposition | As a cashier, I want ticket issuance to be easier |
| Acceptance criteria | <ins>Acceptance criteria 5:</ins><br/>Suppose the customer wants to get to the theater more easily <br/> After issuing the ticket for inspection, the customer will be able to enter the theater faster </ br> And ensure that customers will enter the correct theater printed on the ticket|
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mrs.Huyen|
|Interation | |
|Estimate | |

![The cashier transfers data to the printer to print tickets ](https://gitlab.com/thuhuyenP/21dthd4-2180609318/-/raw/main/Casher%20transfers%20data%20to%20printer%20to%20print%20ticket.png?ref_type=heads)
